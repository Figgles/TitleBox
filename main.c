#include <asm-generic/ioctls.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#define HR 0x2500
#define VR 0x2502
#define TL 0x250c
#define TR 0x2510
#define BL 0x2514
#define BR 0x2518

int getx();
int getStrStart(int w, int h, int m_len);
void memset32(int* s, int x, size_t n);
void clear();
void put_box(int w, int h, const char* message);
void put_box_c(int w, int h, const char* message);

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "");

	if(argc != 2) {
		fprintf(stderr, "usage: %s <message>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	int x = 0;
	int y = 3;
	int w = getx()/2;
	for (int i = 0; i < w; i++) {
		usleep(300000/w);
		put_box_c(i, y, argv[1]);
	}

	return 0;
}

int getx() {
	static struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	 return w.ws_col;
}

int getStrStart(int w, int h, int m_len) {
	return h/2*w + w/2 - m_len/2;
}

void memset32(int* s, int x, size_t n) {
	while(n) {
		s[--n] = x;
	}
}

void clear() {
	printf("\x1b[2J");
	printf("\x1b[H");
}

void put_box(int w, int h, const char* message) {
	int canvas[w*h];
	memset32(canvas, ' ', w*h);

	// Set corners
	canvas[0] = TL;
	canvas[w-1] = TR;
	canvas[h*w-w] = BL;
	canvas[w*h-1] = BR;

	// Set horizontals
	for(int i = 1; i < w-1; i++) {
		canvas[i] = HR;
		canvas[(h-1)*w+i] = HR;
	}
		
	// Set Verticals
	for(int i = 2*w-1; i < w*h-1; i+=w) {
		canvas[i] = VR;
		canvas[i-(w-1)] = VR;
	}

	// add message
	int m_len = strlen(message);
	for(int i = 0; i < m_len; i++) {
		if ( getStrStart(w, h, m_len) + i > h/2*w && getStrStart(w,h,m_len)+i < h/2*w+w-1)
			canvas[getStrStart(w, h, m_len)+i] = message[i];
	}
		
	clear();
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			printf("%lc", canvas[y*w+x]);
		}
		putc('\n', stdout);
	}
}
void put_box_c(int w, int h, const char* message) {
	int cw = getx();
	int canvas[cw*h];
	memset32(canvas, ' ', cw*h);

	// Set corners
	canvas[cw/2-w/2] = TL;
	canvas[cw/2+w/2] = TR;
	canvas[h*cw-cw/2-w/2] = BL;
	canvas[h*cw-cw/2+w/2] = BR;

	// Set horizontals
	for(int i = cw/2-w/2+1; i < cw/2+w/2; i++) {
		canvas[i] = HR;
		canvas[(h-1)*cw+i] = HR;
	}
		
	// Set Verticals
	for(int i = 2*cw-cw/2-w/2; i < h*cw-cw/2-w/2; i+=cw) {
		canvas[i] = VR;
		canvas[i+((w>>1)<<1)] = VR;
	}

	// add message
	int m_len = strlen(message);
	for(int i = 0; i < m_len; i++) {
		if ( getStrStart(cw, h, m_len) + i > h/2*cw && getStrStart(cw,h,m_len)+i < h/2*cw+(cw+w)/2)
			canvas[getStrStart(cw, h, m_len)+i] = message[i];
	}
		
	clear();
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < cw; x++) {
			printf("%lc", canvas[y*cw+x]);
		}
		putc('\n', stdout);
	}
}
